;; Set the package installation directory so that packages aren't stored in the
;; ~/.emacs.d/elpa path.
(require 'package)
(setq package-user-dir (expand-file-name "./.packages"))
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

;; Initialize the package system
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Install dependencies
(package-install 'htmlize)

;; Load the publishing system
(require 'ox-publish)

(setq blog-postamble
      "
<footer>
  <div>
    <div>
      Copyright &copy; 2023-2025 ilcastagnere.it  alcuni diritti riservati<br/>
      I contenuti sono disponibili sotto licenza
      <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\">
        CC-BY-SA 4.0
      </a> salvo diversamente disposto.
    </div>
  </div> 
  <div>
    Sito creato con %c su <a href=\"https://www.gnu.org\">GNU</a>/<a href=\"https://www.kernel.org/\">Linux</a>
  </div>
</footer>
")


;; Customize the HTML output
(setq org-html-validation-link nil            ;; Don't show validation link
      org-html-doctype "html5"
      org-html-html5-fancy t
      org-html-head-include-scripts nil       ;; Use our own scripts
      org-html-head-include-default-style nil ;; Use our own styles
      org-html-postamble blog-postamble)

(setq org-publish-project-alist
      (list '("main"
              :base-directory "./content"
              :base-extension "org"
              :publishing-directory "./public"
              :publishing-function org-html-publish-to-html
              :headline-levels 6
              :with-timestamps t
	      :with-author nil           ;; Don't include author name
              :with-creator t            ;; Include Emacs and Org versions in footer
              :with-toc t
              :section-numbers nil
	      :recursive t)
            '("assets"
              :base-directory "./content/assets"
	      :base-extension any
	      ;;:base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|woff2\\|ttf"
              :publishing-directory "./public/assets"
              :recursive t
              :publishing-function org-publish-attachment)))

;; Generate the site output
(org-publish-all t)

(message "Build complete!")
